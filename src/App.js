import React from 'react';

import { withAuthenticator } from 'aws-amplify-react';
import { UploadForm } from './UploadForm';

import Amplify from '@aws-amplify/core';
import config from './aws-exports';
import { ImagesList } from './ImagesList';

Amplify.configure(config);

function App() {
  return (
    <div style={{ width: 600, margin: '0 auto' }}>
      <UploadForm />
      <ImagesList />
    </div>
  );
}

export default withAuthenticator(App);
