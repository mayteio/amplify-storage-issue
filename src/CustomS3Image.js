import React, { useEffect, useState } from 'react';
import { Storage } from 'aws-amplify';

export const CustomS3Image = ({ file }) => {
  const [image, setImage] = useState();
  useEffect(() => {
    Storage.get(file.key, {
      level: 'protected',
      identityId: file.identityId,
    })
      .then(setImage)
      .catch(err => console.log(err));
  }, [file]);

  return image ? <img src={image} alt="" /> : 'Nothing from Storage.get...';
};
