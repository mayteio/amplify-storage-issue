import React, { useEffect, useState } from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import { listImages } from './graphql/queries';
import { S3Image } from 'aws-amplify-react';
import { CustomS3Image } from './CustomS3Image';

export const ImagesList = () => {
  const [images, setImages] = useState();

  // load images
  useEffect(() => {
    const getImages = async () => setImages(await API.graphql(graphqlOperation(listImages)));
    getImages();
  }, []);

  console.log(images);

  return (
    <div>
      {images &&
        images.data.listImages.items.map(image => (
          <React.Fragment key={image.id}>
            <S3Image
              level="protected"
              identityId={image.file[0].identityId}
              imgKey={image.file[0].key}
            />
            {/* Same issue if we build our own implementation */}
            <CustomS3Image file={image.file[0]} />

            {image.title}
          </React.Fragment>
        ))}
    </div>
  );
};

// https://amplifystorageissue840ba20732494781afc4a8783754100601-develop.s3-ap-southeast-2.amazonaws.com/protected/ap-southeast-2%3A2201d300-5134-4538-83ec-911d281d3eab/2.032109714444997Screen+Shot+2020-02-24+at+9.48.35+am.png
