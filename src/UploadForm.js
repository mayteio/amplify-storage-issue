import React, { useState } from 'react';
import { Formik } from 'formik';
import Auth from '@aws-amplify/auth';
import Storage from '@aws-amplify/storage';
import API, { graphqlOperation } from '@aws-amplify/api';
import { createImage } from './graphql/mutations';

export const UploadForm = () => {
  const [keys, setKeys] = useState([]);

  return (
    <Formik
      onSubmit={async ({ title }, { resetForm }) => {
        try {
          const { identityId } = await Auth.currentCredentials();
          const result = await API.graphql(
            graphqlOperation(createImage, {
              input: { title, file: keys.map(key => ({ ...key, identityId })) },
            })
          );
          console.log(result);
          resetForm();
        } catch (error) {
          console.log(error);
        }
      }}
      initialValues={{ title: '', file: '' }}
    >
      {({ values, handleChange, handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <div style={{ marginBottom: 16 }}>
            <input
              type="text"
              value={values.title}
              onChange={handleChange}
              name="title"
              placeholder="Image title"
            />
          </div>
          <div style={{ marginBottom: 16 }}>
            <input
              type="file"
              onChange={async event => {
                const keys = await Promise.all(
                  Array.from(event.target.files).map(file =>
                    Storage.put(Math.random() * Math.PI + file.name, file, {
                      level: 'protected',
                    })
                  )
                );
                setKeys(keys);
              }}
              name="file"
            />
          </div>
          <button type="submit" disabled={!keys.length}>
            Submit
          </button>
        </form>
      )}
    </Formik>
  );
};
